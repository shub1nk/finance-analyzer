import React from 'react';
import { render } from 'react-dom';

const App = () => {
  return <section>Анализатор финансов</section>
}

render(
	<App />,
	document.getElementById('root')
);
